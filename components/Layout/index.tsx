import { ReactNode } from 'react';
import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import styles from './Layout.module.css';
import Header from '../Header';
import Sidemenu from '../Sidemenu';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

interface LayoutProps {
    children: ReactNode;
}
export default function Layout(props: LayoutProps) {
  const { children } = props;
  return (
    <Container maxWidth="false" className={styles.container}>
      <Box sx={{ flexGrow: 1 }}>
        <Grid container spacing={2}>
          <Grid item xs={3}>
            <Item>
              <Sidemenu />
            </Item>
          </Grid>
          <Grid item xs={9}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Item>
                  <Header />
                </Item>
              </Grid>
              <Grid item xs={12}>
                <Item>
                  <div>{children}</div>
                </Item>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
}
