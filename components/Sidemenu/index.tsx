import * as React from 'react';
// import ListSubheader from '@mui/material/ListSubheader';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import DashboardRoundedIcon from '@mui/icons-material/DashboardRounded';
import AssignmentRoundedIcon from '@mui/icons-material/AssignmentRounded';
import GroupRoundedIcon from '@mui/icons-material/GroupRounded';
import AccountTreeRoundedIcon from '@mui/icons-material/AccountTreeRounded';
import AdminPanelSettingsRoundedIcon from '@mui/icons-material/AdminPanelSettingsRounded';
import StorageRoundedIcon from '@mui/icons-material/StorageRounded';
import SupervisedUserCircleRoundedIcon from '@mui/icons-material/SupervisedUserCircleRounded';

export default function NestedList() {
  const [openGeneral, setGeneralOpen] = React.useState(false);
  const [openPm, setPmOpen] = React.useState(false);
  const [openPmo, setPmoOpen] = React.useState(false);
  const [openHr, setHrOpen] = React.useState(false);
  const [openMasterData, setMasterDataOpen] = React.useState(false);
  const [openUserRole, setUserRoleOpen] = React.useState(false);

  const GeneralClick = () => {
    setGeneralOpen(!openGeneral);
  };
  const PmClick = () => {
    setPmOpen(!openPm);
  };
  const PmoClick = () => {
    setPmoOpen(!openPmo);
  };
  const HrClick = () => {
    setHrOpen(!openHr);
  };
  const MasterDataClick = () => {
    setMasterDataOpen(!openMasterData);
  };
  const UserRoleClick = () => {
    setUserRoleOpen(!openUserRole);
  };

  return (
    <List
      sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}
      component="nav"
      aria-labelledby="nested-list-subheader"
    >
      <ListItemButton>
        <ListItemIcon>
          <DashboardRoundedIcon />
        </ListItemIcon>
        <ListItemText primary="Dashboard" />
      </ListItemButton>
      <ListItemButton onClick={GeneralClick}>
        <ListItemIcon>
          <AssignmentRoundedIcon />
        </ListItemIcon>
        <ListItemText primary="General" />
        {openGeneral ? <ExpandLess /> : <ExpandMore />}
      </ListItemButton>
      <Collapse in={openGeneral} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="My Task List" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="My Project" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="My Leave" />
          </ListItemButton>
        </List>
      </Collapse>
      <ListItemButton onClick={PmClick}>
        <ListItemIcon>
          <GroupRoundedIcon />
        </ListItemIcon>
        <ListItemText primary="Project Manager" />
        {openPm ? <ExpandLess /> : <ExpandMore />}
      </ListItemButton>
      <Collapse in={openPm} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="My Project" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="My Approval" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Monitoring Development" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Report PM" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Report Approval" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Approval Leave" />
          </ListItemButton>
        </List>
      </Collapse>
      <ListItemButton onClick={PmoClick}>
        <ListItemIcon>
          <AccountTreeRoundedIcon />
        </ListItemIcon>
        <ListItemText primary="PMO" />
        {openPmo ? <ExpandLess /> : <ExpandMore />}
      </ListItemButton>
      <Collapse in={openPmo} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="All Project" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Monitoring Developer" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Monitoring Development" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Monitoring PM" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Idle Prediction" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="PMO Approval" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Report PMO" />
          </ListItemButton>
        </List>
      </Collapse>
      <ListItemButton onClick={HrClick}>
        <ListItemIcon>
          <AdminPanelSettingsRoundedIcon />
        </ListItemIcon>
        <ListItemText primary="Human Resource" />
        {openHr ? <ExpandLess /> : <ExpandMore />}
      </ListItemButton>
      <Collapse in={openHr} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="All Approval" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Report HRIS" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Report WFH" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Permission" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Summary User Per Day" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Summary User Per Month" />
          </ListItemButton>
        </List>
      </Collapse>
      <ListItemButton onClick={MasterDataClick}>
        <ListItemIcon>
          <StorageRoundedIcon />
        </ListItemIcon>
        <ListItemText primary="Master Data" />
        {openMasterData ? <ExpandLess /> : <ExpandMore />}
      </ListItemButton>
      <Collapse in={openMasterData} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Holiday" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Jobs" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Clients" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Sysparam" />
          </ListItemButton>
        </List>
      </Collapse>
      <ListItemButton onClick={UserRoleClick}>
        <ListItemIcon>
          <SupervisedUserCircleRoundedIcon />
        </ListItemIcon>
        <ListItemText primary="User & Role" />
        {openUserRole ? <ExpandLess /> : <ExpandMore />}
      </ListItemButton>
      <Collapse in={openUserRole} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Holiday" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Jobs" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Clients" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 9 }}>
            <ListItemText primary="Sysparam" />
          </ListItemButton>
        </List>
      </Collapse>
    </List>
  );
}
